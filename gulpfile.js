var browsers     = '> 1%, last 2 versions, IE >= 8, Firefox ESR, Opera 12.1';
var config       = require('./gulpconfig.json');
var browserslist = require('browserslist');
var gulp         = require('gulp');
var gutil        = require('gulp-util');
var when         = require('gulp-if');
var plumber      = require('gulp-plumber');
var rename       = require('gulp-rename');
var replace      = require('gulp-replace');
var less         = require('gulp-less');
var autoprefix   = require('gulp-autoprefixer');
var group        = require('gulp-group-css-media-queries');
var minify       = require('gulp-cssnano');
var include      = require('gulp-include');
var uglify       = require('gulp-uglify');
var inquirer     = require('inquirer');
var yargs        = require('yargs').default({ 't' : 'default' });
var argv         = require('yargs').argv;
var WP           = require('wp-cli');

function getFileName(path) {
   return path.split(/[\\/]/).pop();
}

function getDirName(path) {
    parts = path.split(/[\\/]/);
    parts.pop();

    return parts.join('/');
}

function log(status, task, t) {
    gutil.log(status + " '" + gutil.colors.cyan(task + "::" + t) + "'");
}

var TaskHandler = {
    wordpress: function(done) {
        WP.discover({path:'.'}, function(WP){
            gutil.log(gutil.colors.cyan('Downloading latest WordPress version...'));
            WP.core.download(function(error, result){
                if (error) {
                    gutil.log(gutil.colors.red(error));
                    done();
                } else {
                    gutil.log(gutil.colors.cyan('Success: WordPress downloaded.'));
                    inquirer.prompt([
                        {
                            type: 'input',
                            name: 'DB_NAME',
                            message: 'DB_NAME:',
                            validate: function(input){
                                return (!input)? 'DB_NAME is required.' : true ;
                            }
                        },
                        {
                            type: 'input',
                            name: 'DB_USER',
                            message: 'DB_USER:',
                            default: 'root'
                        },
                        {
                            type: 'input',
                            name: 'DB_PASSWORD',
                            message: 'DB_PASSWORD:',
                            default: 'root'
                        },
                        {
                            type: 'input',
                            name: 'DB_HOST',
                            message: 'DB_HOST:',
                            default: 'localhost'
                        }
                    ], function(obj) {
                        gulp.src('wp-config-sample.php')
                            .pipe(replace('database_name_here', obj.DB_NAME))
                            .pipe(replace('username_here', obj.DB_USER))
                            .pipe(replace('password_here', obj.DB_PASSWORD))
                            .pipe(replace('localhost', obj.DB_HOST))
                            .pipe(replace('false', true))
                            .pipe(rename('wp-config.php'))
                            .pipe(gulp.dest('.'))
                        ;
                        gutil.log(gutil.colors.cyan('Success: Generated wp-config.php file.'))
                        done();
                    });
                }
            });
        });
    },
    less: function(t) {
        log('Starting', 'less', t);
        for(src in config.less[t]) {
            dest = config.less[t][src];
            if (dest.constructor !== Array) dest = Array(dest);
            dest.forEach(function(path){
                var name = getFileName(path);
                var dest = getDirName(path);
                gulp.src(src)
                    .pipe(plumber(function(error) {
                        gutil.log(gutil.colors.red('Error: ' + error.message));
                        this.emit('end');
                    }))
                    .pipe(less())
                    .pipe(group())
                    .pipe(when((name.indexOf('.min.') !== -1), minify({
                        keepSpecialComments: false
                    })))
                    .pipe(autoprefix(browsers))
                    .pipe(rename(name))
                    .pipe(gulp.dest(dest))
                ;
            });
        }
        log('Finished', 'less', t);
    },
    js: function(t) {
        log('Starting', 'js', t);
        for(src in config.js[t]) {
            dest = config.js[t][src];
            if (dest.constructor !== Array) dest = Array(dest);
            dest.forEach(function(path){
                var name = getFileName(path);
                var dest = getDirName(path);
                gulp.src(src)
                    .pipe(plumber(function(error) {
                        gutil.log(gutil.colors.red('Error: ' + error.message));
                        this.emit('end');
                    }))
                    .pipe(include())
                    .pipe(when((name.indexOf('.min.') !== -1), uglify()))
                    .pipe(rename(name))
                    .pipe(gulp.dest(dest))
                ;
            });
        }
        log('Finished', 'js', t);
    }
}

gulp.task('watch', function(done) {
    config.watch.forEach(function(observed){
        gulp.watch(observed.src, function(){
            Object.keys(observed.run).forEach(function(task){
                TaskHandler[task](observed.run[task]);
            });
        });
    });
});

gulp.task('wordpress', function(done) {
    TaskHandler.wordpress(done);
});

gulp.task('less', function() {
    TaskHandler.less(argv.t);
});

gulp.task('js', function() {
    TaskHandler.js(argv.t);
});
